comet.data\_dir package
=======================

Module contents
---------------

.. automodule:: comet.data_dir
   :members:
   :undoc-members:
   :show-inheritance:
