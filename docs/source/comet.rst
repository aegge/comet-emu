PTEmu
------------------

.. automodule:: comet.PTEmu
   :members:
   :undoc-members:
   :show-inheritance:

bispectrum
-----------------------

.. automodule:: comet.bispectrum
   :members:
   :undoc-members:
   :show-inheritance:

cosmology
----------------------

.. automodule:: comet.cosmology
   :members:
   :undoc-members:
   :show-inheritance:

data
-----------------

.. automodule:: comet.data
   :members:
   :undoc-members:
   :show-inheritance:

tables
-------------------

.. automodule:: comet.tables
   :members:
   :undoc-members:
   :show-inheritance:
